# -*- coding: utf-8 -*-

"""setup.py: setuptools control."""

from setuptools import setup

setup(
    name = "cmd-trackman",
    test_suite = 'tests',
    packages = ["trackman"],
    entry_points = {
        "console_scripts": ['trackman =trackman.scheduler:main']
        },
    version = '0.1',
    description = "Schedule talks in a conference",
    author = "Deepankar Bajpeyi",
    author_email = "dbajpeyi@gmail.com",
    )
