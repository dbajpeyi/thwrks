import unittest
import tests

def my_module_suite():
    loader = unittest.TestLoader()
    suite = loader.loadTestsFromModule(tests)
    return suite
