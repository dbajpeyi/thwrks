import unittest
from trackman.scheduler import * 

class TestScheduler(unittest.TestCase):

    def setUp(self):
        file_path = 'tests/data_test/testdata.txt'
        self.scheduler = Scheduler(file_path)
        self.talk_str = "Writing Fast Tests Against Enterprise Rails 60min"
        self.lightning = "Rails for Python Developers lightning"

    def test_parse_talk(self):
        text, duration = self.scheduler._parse_talk(self.talk_str)
        self.assertEqual(duration, '60')

    def test_parse_talk_ligtening(self):
        text, duration = self.scheduler._parse_talk(self.lightning)
        self.assertEqual(duration, '5')

    def test_sum(self):
        seq = ('A World Without HackerNews', 'Ruby on Rails Legacy App Maintenance')
        talks = self.scheduler.remaining
        self.assertEqual(self.scheduler._sum(seq, talks), 90)

    def test_morning_talks(self):
    	self.assertEqual(self.scheduler.get_morning_talks(self.scheduler.remaining), {})

