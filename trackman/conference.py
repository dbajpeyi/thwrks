from datetime import datetime, timedelta, time

class Talk(object):
    """
    Class defining a talk or a proposal to be scheduled.
    Each talk has a title and a duration for which it will last.
    Duration is in minutes
    """

    def __init__(self, title, timelength, session):
        super(Talk, self).__init__()
        self.title = title
        self.duration = timedelta(minutes=int(timelength))
        self.session = session

    def __unicode__(self):
        return "%s %s" % (self.title, self.duration)


class Conference(object):
    """Conference object defining a conference"""
    def __init__(self, name):
        super(Conference, self).__init__()
        self.name = name

    def __unicode__(self):
        return self.name
        

class Track(object):
    """
    Class definition for a track.
    """

    def __init__(self, name, conference):
        super(Track, self).__init__()
        self.name = name
        self.conference = conference

    def __unicode__(self):
        return "%s %s" % (self.name)

class Session(object):

    def __init__(self, s_type, track):
        super(Session, self).__init__()
        self.track = track
        self.s_type = s_type
        if s_type == 'Morning':
            self.start_time = time(9,0,0)
            self.end_time = time(12, 0, 0)
        elif s_type == 'Afternoon':
            self.start_time = time(13,0,0)
        else:
            raise ValueError('s_type can only be "Morning" or "Afternoon"')

    def __unicode__(self):
        return "%s in %s" % ("\n".join(self.talks) ,self.s_type)

