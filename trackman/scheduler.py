
import re
from conference import *
import itertools
from datetime import time, timedelta, datetime
import argparse

class Scheduler(object):
    """
    This class initializes a schedule based on a file 
    input with a list of talks and duration
    """

    def _initialize_talks(self, filepath):
        with open(filepath,'r') as f:
            inp = f.readlines()
            lines = (i.strip('\n') for i in inp)
        return lines

    def _parse_talk(self, talk):
        try:
            text, duration, minute = re.findall('\d+|\D+', talk)
        except ValueError:
            text = talk.split('lightning')[0]
            duration = '5'

        return text.strip(" "), duration

    def _read_talks(self, talklist):
        talks = {}
        for talk in talklist:
            text, duration = self._parse_talk(talk)
            talks[text] = duration
        return talks

    def __init__(self, filepath):
        super(Scheduler, self).__init__()
        self.cur_time = time(9, 0, 0)
        self.conference = Conference('ThoughtWorks')
        self.tracks = [Track('Track 1', self.conference), Track('Track 2', self.conference)]
        self.sessions = []
        for track in self.tracks:
            self.sessions.append(Session('Morning', track))
            self.sessions.append(Session('Afternoon', track))

        self.talks = self._read_talks(self._initialize_talks(filepath))
        self.remaining = self.talks
        self.talk_objs = []
        for session in self.sessions:
            if session.s_type == 'Morning':
                morning_talks = self.get_morning_talks(self.remaining)
                for k,v in morning_talks.iteritems():
                    self.talk_objs.append(Talk(k,v,session))
            elif session.s_type == 'Afternoon':
                evening_talks = self.get_afternoon_talks(self.remaining)
                for k,v in evening_talks.iteritems():
                    self.talk_objs.append(Talk(k,v,session))


    def _sum(self, seq, talks):
        sum_ = 0
        for s in seq:
            if talks.get(s):
                sum_ += int(talks[s])
        return sum_


    def get_morning_talks(self, talks):
        morning_talks = {}
        for i in xrange(len(talks.keys()), 0, -1):
            for seq in itertools.combinations(talks ,i):
                if self._sum(seq, talks) == 180:
                    morning_talks = {k:v for k,v in talks.iteritems() if k in seq}
        self.remaining = {k:v for k,v in talks.items() if k not in morning_talks}
        return morning_talks

    def get_afternoon_talks(self, talks):
        max_talks = []
        evening_talks = {}
        for i in xrange(len(talks.keys()), 0, -1):
            for seq in itertools.combinations(talks ,i):
                if self._sum(seq, talks) > 180 and self._sum(seq, talks) <= 240 and len(seq) > len(max_talks):
                    max_talks = seq
                    evening_talks = {k:v for k,v in talks.iteritems() if k in seq}
        self.remaining = {k:v for k,v in talks.items() if k not in evening_talks}
        return evening_talks

    def _schedule_networking(self, schedule):
        for track in self.tracks:
            last_obj = schedule[track.name][-1]
            schedule[track.name].append({
                'title' : 'Networking', 
                'time' : (datetime.combine(datetime.today(), last_obj.get('time')) + \
                     last_obj.get('duration')).time()
                 })

    def _schedule(self):
        from collections import OrderedDict
        schedule = OrderedDict()

        for track in self.tracks:
            schedule[track.name] = []

        for talk in self.talk_objs:
            if datetime.combine(datetime.today(), time(17,0,0)) - \
                datetime.combine(datetime.today(),self.cur_time) < talk.duration:
                self.cur_time = time(9,0,0)
            elif self.cur_time == time(12,0,0):
                schedule[talk.session.track.name].append({
                'title' : 'Lunch',
                'duration' : timedelta(minutes=60),
                'time' : self.cur_time
               })
                self.cur_time = time(13,0,0)
            schedule[talk.session.track.name].append({
                'title' : talk.title, 
                'duration' : talk.duration,
                'time' : self.cur_time
          })
            self.cur_time = (datetime.combine(datetime.today(), self.cur_time) + talk.duration).time()

        self._schedule_networking(schedule)
        return schedule

    def write(self, schedule):
        with open('data/out.txt', 'w+') as f:
            for k,v in schedule.iteritems():
                f.write('---\n')
                f.write("%s\n"%k)
                f.write('---\n')
                for i in v:
                    f.write("%s %s %s\n" % (i.get('time'), i.get('title'), i.get('duration') or ''))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--filepath", help="path to the input file containing talks and duration")
    args = parser.parse_args()
    if not args.filepath:
        print "error : expected --filepath argument"
    else:
        scheduler = Scheduler(args.filepath)
        schedule = scheduler._schedule()
        scheduler.write(schedule)
